=begin
Deoxyribonucleic acid (DNA) is a chemical found in the nucleus of cells and
carries the "instructions" for the development and functioning of living
organisms.
If you want to know more http://en.wikipedia.org/wiki/DNA
In DNA strings, symbols "A" and "T" are complements of each other,
as "C" and "G". You have function with one side of the DNA; you need to get the
other complementary side. DNA strand is never empty or there is no DNA at all
(again, except for Haskell).
DNA_strand ("ATTGC") # return "TAACG"
DNA_strand ("GTAT") # return "CATA"
=end

#some solutions

def DNA_strand(dna)
  dna.tr('ATCG', 'TAGC')
end

def DNA_strand2(dna)
  dna.gsub(/[ATGC]/, 'A' => 'T', 'T' => 'A', 'C' => 'G', 'G' => 'C')
end

def DNA_strand3(dna)
  dna.upcase!
  result = ''
  dna.chars.each do |s|
    case s
    when 'A' then result += 'T'
    when 'T' then result += 'A'
    when 'G' then result += 'C'
    when 'C' then result += 'G'
    else
      puts 'Invalid DNA'
    end
  end
  result
end

def DNA_strand4(dna)
  complement = { 'A' => 'T', 'T' => 'A', 'C' => 'G', 'G' => 'C' }
  dna.gsub(/[ATCG]/) { |base| complement[base] }
end

$pairs = { 'A' => 'T', 'T' => 'A', 'C' => 'G', 'G' => 'C' }
def DNA_strand5(dna)
  dna.chars.map { |c| $pairs[c] }.join
end
